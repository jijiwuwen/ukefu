package com.ukefu.webim.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.ukefu.util.UKTools;

/**
 * 联系人类型
 *
 */
@Entity
@Table(name = "uk_contacts_authorize")
@org.hibernate.annotations.Proxy(lazy = false)
public class ContactsItemAuthorize implements java.io.Serializable {

	private static final long serialVersionUID = 632513201172468772L;

	private String id = UKTools.getUUID();
	private String userid ;
	private String itemid ;
	private String creater ;
	private Date createtime = new Date();
	private String orgi ;
	private boolean sup = false;//是否管理员
	private boolean datastatus ;
	
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getItemid() {
		return itemid;
	}
	public void setItemid(String itemid) {
		this.itemid = itemid;
	}
	public boolean isSup() {
		return sup;
	}
	public void setSup(boolean sup) {
		this.sup = sup;
	}
	public boolean isDatastatus() {
		return datastatus;
	}
	public void setDatastatus(boolean datastatus) {
		this.datastatus = datastatus;
	}
	
	
}
