package com.ukefu.webim.web.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;


/**
 * @author jaddy0302 Rivulet User.java 2010-3-17
 * 
 */
@Entity
@Table(name = "uk_user")
@org.hibernate.annotations.Proxy(lazy = false)
public class DisUser implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
    @Id
	private String id ;
	
	private String username ;
	private String email ;
	private String uname ;
	private String mobile ;
	private String usertype ; // 0 Admin User  : !0  Other User
	
	private boolean superuser = false ;	//是否是超级管理员
	
	private String orgi ;
	private String orgid ;
	private String organ ;
	
	private boolean agent ;	//是否开通坐席功能
	private boolean callcenter ;	//是否启用呼叫中心 坐席功能
	private String skill ;
	private boolean datastatus ;//数据状态，是否已删除	
	
	private Date lastdisdate ;	//最后分配时间
	private int disnum ;		//分配数量
	private Date lastworkorderdate ;	//工单最近分配时间
	private int workordernum ;		//分配工单数量
	
	private boolean workorder ;		//启用工单分配
	private boolean bussopdis ;		//启用商机分配
	
	private String keyword ;		//分类的 主题词
	
	public DisUser(){}
	public DisUser(String id){
		this.id = id ;
	}
	
	
	/**
	 * @return the id
	 */
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	public String getId() {
		return id;
	}
	public String getUsername() {
		return username;
	}
	public String getEmail() {
		return email;
	}
	public String getUname() {
		return uname;
	}
	public String getMobile() {
		return mobile;
	}
	public String getUsertype() {
		return usertype;
	}
	public boolean isSuperuser() {
		return superuser;
	}
	public String getOrgi() {
		return orgi;
	}
	public String getOrgid() {
		return orgid;
	}
	public boolean isAgent() {
		return agent;
	}
	public boolean isCallcenter() {
		return callcenter;
	}
	public String getSkill() {
		return skill;
	}
	public boolean isDatastatus() {
		return datastatus;
	}
	public Date getLastdisdate() {
		return lastdisdate;
	}
	public int getDisnum() {
		return disnum;
	}
	public Date getLastworkorderdate() {
		return lastworkorderdate;
	}
	public int getWorkordernum() {
		return workordernum;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public void setSuperuser(boolean superuser) {
		this.superuser = superuser;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public void setOrgid(String orgid) {
		this.orgid = orgid;
	}
	public void setAgent(boolean agent) {
		this.agent = agent;
	}
	public void setCallcenter(boolean callcenter) {
		this.callcenter = callcenter;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}
	public void setDatastatus(boolean datastatus) {
		this.datastatus = datastatus;
	}
	public void setLastdisdate(Date lastdisdate) {
		this.lastdisdate = lastdisdate;
	}
	public void setDisnum(int disnum) {
		this.disnum = disnum;
	}
	public void setLastworkorderdate(Date lastworkorderdate) {
		this.lastworkorderdate = lastworkorderdate;
	}
	public void setWorkordernum(int workordernum) {
		this.workordernum = workordernum;
	}
	public String getOrgan() {
		return organ;
	}
	public void setOrgan(String organ) {
		this.organ = organ;
	}
	public boolean isWorkorder() {
		return workorder;
	}
	public boolean isBussopdis() {
		return bussopdis;
	}
	public void setWorkorder(boolean workorder) {
		this.workorder = workorder;
	}
	public void setBussopdis(boolean bussopdis) {
		this.bussopdis = bussopdis;
	}
	@Transient
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	
}
